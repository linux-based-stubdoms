XEN_ROOT = $(CURDIR)/..
STUBDOM_ARCH = x86_32

#download linux kernel
LINUX_STUBDOM_GIT_URL ?= git://git.kernel.org/pub/scm/linux/kernel/git/jeremy/xen.git
LINUX_SRCDIR = linux-src-$(STUBDOM_ARCH).git
XEN_GIT_ORIGIN ?= xen
XEN_LINUX_GIT_REMOTEBRANCH ?= xen/stable-2.6.32.x
XEN_LINUX_GIT_LOCALBRANCH ?= $(XEN_LINUX_GIT_REMOTEBRANCH)
XEN_LINUX_GITREV  ?= $(XEN_GIT_ORIGIN)/$(XEN_LINUX_GIT_REMOTEBRANCH)

#build linux-stubdom kernel
IMAGE_TARGET ?= bzImage
LINUX_DIR = build-linux-$(STUBDOM_ARCH)
INSTALL_PATH = install-stubdom-$(STUBDOM_ARCH)
LINUX_ARCH = $$(sh $(CURDIR)/select-linux-arch $(LINUX_SRCDIR) $(STUBDOM_ARCH))

#download upstream qemu
IOEMU_GIT_URL ?= git://xenbits.xen.org/people/sstabellini/qemu-dm.git
IOEMU_SRCDIR = ioemu-src-$(STUBDOM_ARCH).git
IOEMU_GIT_ORIGIN ?= origin
IOEMU_GIT_REMOTEBRANCH ?= xen-stable-0.15
IOEMU_GIT_LOCALBRANCH ?= $(IOEMU_GIT_REMOTEBRANCH)
IOEMU_GITREV ?= $(IOEMU_GIT_ORIGIN)/$(IOEMU_GIT_REMOTEBRANCH)

#libxl
LIBXL_DIR = libxl-$(STUBDOM_ARCH)
LIBXL_SRCDIR = $(XEN_ROOT)/tools/libxl

#download udev
#UDEV_URL?= http://www.us.kernel.org/pub/linux/utils/kernel/hotplug
UDEV_URL?= http://ceos.googlecode.com/files
UDEV_VERSION=100

#download bridge-util
BRIDGE_UTILS_URL ?= http://cdnetworks-kr-2.dl.sourceforge.net/project/bridge/bridge
BRIDGE_UTILS_VERSION = 1.5

#build ioemu
IOEMU_DIR = build-ioemu-$(STUBDOM_ARCH)

#build qemu-tools
QEMU_TOOLS_DIR = qemu-tools
QEMU_TOOLS = qemu-wrapper qemu-ifup netconfig

TARGETS=ioemu

GIT ?= git

WGET=wget -c

.PHONY: all
all: ioemu-stubdom

######################
# download source code
######################

.PHONY: download-linux-src
download-linux-src:
	if ! [ -d $(LINUX_SRCDIR) ]; then \
		rm -rf $(LINUX_SRCDIR) $(LINUX_SRCDIR).tmp; \
		mkdir $(LINUX_SRCDIR).tmp; rmdir $(LINUX_SRCDIR).tmp; \
		$(GIT) clone -o $(XEN_GIT_ORIGIN) -n $(LINUX_STUBDOM_GIT_URL) $(LINUX_SRCDIR).tmp; \
		(cd $(LINUX_SRCDIR).tmp; git checkout -b $(XEN_LINUX_GIT_LOCALBRANCH) $(XEN_LINUX_GITREV); git checkout); \
		mv $(LINUX_SRCDIR).tmp $(LINUX_SRCDIR); \
		patch -d $(LINUX_SRCDIR) -p1 < linux-src.patch; \
	fi

.PHONY: download-ioemu-src
download-ioemu-src:
	if ! [ -d $(IOEMU_SRCDIR) ]; then \
		rm -rf $(IOEMU_SRCDIR) $(IOEMU_SRCDIR).tmp; \
		mkdir $(IOEMU_SRCDIR).tmp; rmdir $(IOEMU_SRCDIR).tmp; \
		$(GIT) clone -o $(IOEMU_GIT_ORIGIN) -n $(IOEMU_GIT_URL) $(IOEMU_SRCDIR).tmp; \
		(cd $(IOEMU_SRCDIR).tmp; git checkout -b $(IOEMU_GIT_LOCALBRANCH) $(IOEMU_GITREV); git checkout);\
		mv $(IOEMU_SRCDIR).tmp $(IOEMU_SRCDIR);\
	fi

################
# linux-stubdom
################

TARGETS_LINUX=$(addprefix linux-stubdom-$(STUBDOM_ARCH)-,$(TARGETS))
LINUX_STUBDOM_CONFIG=$(addprefix linux-config-,$(TARGETS))
MAKE_RAMDISK_SCRIPT=$(addprefix mk-ramdisk-,$(TARGETS))
$(TARGETS_LINUX):download-linux-src
	mkdir -p $@
	mkdir -p $@/$(LINUX_DIR)
	mkdir -p $@/$(INSTALL_PATH)
	if [ ! -f $@/$(LINUX_DIR)/.config ]; then \
		cp $(LINUX_STUBDOM_CONFIG) $@/$(LINUX_DIR)/.config;\
		$(MAKE) -C $(LINUX_SRCDIR) ARCH=$(LINUX_ARCH) oldconfig O=$$(/bin/pwd)/$@/$(LINUX_DIR);\
		$(MAKE) -C $@/$(LINUX_DIR) ARCH=$(LINUX_ARCH) prepare;\
	fi
	if grep "^CONFIG_MODULES=" $@/$(LINUX_DIR)/.config ; then \
	    $(MAKE) -C $@/$(LINUX_DIR) ARCH=$(LINUX_ARCH) modules || exit 1 ; \
	    $(MAKE) -C $@/$(LINUX_DIR) ARCH=$(LINUX_ARCH) INSTALL_MOD_PATH=$(CURDIR)/$@/$(INSTALL_PATH) modules_install ; \
	fi
	$(MAKE) -C $@/$(LINUX_DIR) ARCH=$(LINUX_ARCH) INSTALL_PATH=$@/$(INSTALL_PATH) $(IMAGE_TARGET)
	@cp -v $@/$(LINUX_DIR)/arch/$(LINUX_ARCH)/boot/bzImage $@/$(INSTALL_PATH)/vmlinuz-$(TARGETS)
	@cp -v $@/$(LINUX_DIR)/.config $@/$(INSTALL_PATH)/config-$(TARGETS)
	@cp -v $@/$(LINUX_DIR)/System.map $@/$(INSTALL_PATH)/System.map-$(TARGETS)

#######
# ioemu
#######

linux-stubdom-$(STUBDOM_ARCH)-ioemu/.link-ioemu-dir:
	mkdir -p linux-stubdom-$(STUBDOM_ARCH)-ioemu/$(IOEMU_DIR)
	set -e;	\
	cd linux-stubdom-$(STUBDOM_ARCH)-ioemu/$(IOEMU_DIR); \
	(cd $(CURDIR)/$(IOEMU_SRCDIR) && find * -type d -print) | xargs mkdir -p;			\
	(cd $(CURDIR)/$(IOEMU_SRCDIR) && find *	! -type l  -type f  $(addprefix ! -name ,	\
			'*.[oda1]' 'config-*' config.mak	\
			'*.html' '*.pod'					\
			)) |							\
   		 while read f; do rm -f "$$f"; ln -s "$(CURDIR)/$(IOEMU_SRCDIR)/$$f" "$$f"; done
#	patch -d linux-stubdom-$(STUBDOM_ARCH)-ioemu/$(IOEMU_DIR) -p1 < ioemu-stubdom.patch
	touch linux-stubdom-$(STUBDOM_ARCH)-ioemu/.link-ioemu-dir

.PHONY: ioemu
ioemu: download-ioemu-src linux-stubdom-$(STUBDOM_ARCH)-ioemu/.link-ioemu-dir
	$(warning "build upstream qemu")
	(cd linux-stubdom-$(STUBDOM_ARCH)-ioemu/$(IOEMU_DIR); \
	sh configure --target-list=i386-softmmu --enable-xen --disable-werror --enable-stubdom \
		--extra-cflags=-I$(XEN_ROOT)/dist/install/usr/include --extra-ldflags=-L$(XEN_ROOT)/dist/install/usr/lib; \
	$(MAKE);\
	cp -af i386-softmmu/qemu ../$(INSTALL_PATH))

#######
# libxl
#######

.link-libxl-dir:
	mkdir -p $(LIBXL_DIR)
	set -e;	\
	cd $(LIBXL_DIR); \
	(cd $(LIBXL_SRCDIR) && $(MAKE) clean);\
	(cd $(LIBXL_SRCDIR) && find *	! -type l  -type f  $(addprefix ! -name ,	\
			'*.[oda1]' 'config-*' config.mak	\
			'*.html' '*.pod'					\
			)) |							\
   		 while read f; do rm -f "$$f"; ln -s "$(LIBXL_SRCDIR)/$$f" "$$f"; done
#	patch -d $(LIBXL_DIR) -p3 < libxl.patch
	touch .link-libxl-dir

.PHONY: libxl
libxl: .link-libxl-dir
	(cd $(LIBXL_DIR);$(MAKE) install;)

######
# udev	
######

udev-$(UDEV_VERSION).tar.tar:
	$(WGET) $(UDEV_URL)/$@

.PHONY: udev
udev: udev-$(UDEV_VERSION).tar.tar
	tar xjvf $<
	( cd udev-$(UDEV_VERSION); \
	  $(MAKE) ; \
	  cp -af udevd ../linux-stubdom-$(STUBDOM_ARCH)-ioemu/$(INSTALL_PATH);\
	  cp -af udevstart ../linux-stubdom-$(STUBDOM_ARCH)-ioemu/$(INSTALL_PATH))

##############
# bridge-utils	
##############

bridge-utils-$(BRIDGE_UTILS_VERSION).tar.gz:
	$(WGET) $(BRIDGE_UTILS_URL)/$@

.PHONY: bridge-utils
bridge-utils: bridge-utils-$(BRIDGE_UTILS_VERSION).tar.gz
	tar zxvf $<
	( cd bridge-utils-$(BRIDGE_UTILS_VERSION); \
	  autoreconf;\
	  ./configure;\
	  $(MAKE);\
	  cp -af ./brctl/brctl ../linux-stubdom-$(STUBDOM_ARCH)-ioemu/$(INSTALL_PATH))

###############
# qemu-tools
###############

.PHONY: qemu-tools
qemu-tools:
	( cd $(QEMU_TOOLS_DIR); \
		$(MAKE) XEN_ROOT=$(XEN_ROOT);\
		cp -af $(QEMU_TOOLS) ../linux-stubdom-$(STUBDOM_ARCH)-ioemu/$(INSTALL_PATH))

###########################
# functional-linux-stubdoms
###########################

.PHONY: ioemu-stubdom
ioemu-stubdom: libxl linux-stubdom-$(STUBDOM_ARCH)-ioemu ioemu qemu-tools udev bridge-utils
	sh $(MAKE_RAMDISK_SCRIPT) linux-stubdom-$(STUBDOM_ARCH)-ioemu/$(INSTALL_PATH) ramdisk-$(TARGETS) ioemu

.PHONY: xenstore-stubdom
xenstore-linux-stubdom:
	$(warning "xenstore-linux-stubdom")


#########
# install
#########
include $(XEN_ROOT)/config/StdGNU.mk

.PHONY:install
install: install-ioemu

install-ioemu: ioemu-stubdom
	$(INSTALL_DIR) "$(XENFIRMWAREDIR)"
	$(INSTALL_DATA) linux-stubdom-$(STUBDOM_ARCH)-ioemu/$(INSTALL_PATH)/vmlinuz-ioemu "$(XENFIRMWAREDIR)/vmlinuz-ioemu"
	$(INSTALL_DATA) linux-stubdom-$(STUBDOM_ARCH)-ioemu/$(INSTALL_PATH)/ramdisk-ioemu "$(XENFIRMWAREDIR)/ramdisk-ioemu"

#######
# clean
#######

# Only clean the build stuff
.PHONY: clean
clean:
	rm -fr linux-stubdom-$(STUBDOM_ARCH)-ioemu
	rm -fr udev-$(UDEV_VERSION)
	rm -fr bridge-utils-$(BRIDGE_UTILS_VERSION)
	rm -fr $(LIBXL_DIR) .link-libxl-dir
	(cd $(QEMU_TOOLS_DIR); $(MAKE) clean)

.PHONY: downloadclean
downloadclean: clean
	rm -fr $(LINUX_SRCDIR)
	rm -fr $(IOEMU_SRCDIR)
	rm -fr udev-$(UDEV_VERSION).tar.bz2

#include <stdio.h>
#include <stdlib.h>
#include <xs.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mount.h>

#include <dirent.h>

#define MAX_GUEST_CMDLINE 1024
#define PARSE_ARGS(ARGS,START,QUOTE,END) \
    c = ARGS; \
    quote = 0; \
    while (*c) { \
	if (*c != ' ') { \
	    START; \
	    while (*c) { \
		if (quote) { \
		    if (*c == quote) { \
			quote = 0; \
			QUOTE; \
			continue; \
		    } \
		} else if (*c == ' ') \
		    break; \
		if (*c == '"' || *c == '\'') { \
		    quote = *c; \
		    QUOTE; \
		    continue; \
		} \
		c++; \
	    } \
	} else { \
            END; \
	    while (*c == ' ') \
		c++; \
	} \
    } \
    if (quote) {\
	printf("Warning: unterminated quotation %c\n", quote); \
	quote = 0; \
    }
#define PARSE_ARGS_COUNT(ARGS) PARSE_ARGS(ARGS, argc++, c++, )
#define PARSE_ARGS_STORE(ARGS) PARSE_ARGS(ARGS, argv[argc++] = c, memmove(c, c + 1, strlen(c + 1) + 1), *c++ = 0)

struct xs_handle *xsh=NULL;

int xenbus_read_integer(const char *path)
{
    char *buf;
    int t;
	unsigned int len;

    buf = xs_read(xsh,XBT_NULL, path, &len);
    if (buf == NULL) {
		printf("Failed to read %s.\n", path);
		free(buf);
		return -1;
    }
    sscanf(buf, "%d", &t);
    free(buf);
    return t;
}

int main(void)
{
	char *c, quote;
	char *domargs;
    int argc;
    char **argv;
    char *envp[] = { NULL };
    char *vm;
    char path[128];
    int domid,fd,i;
	unsigned int len;
	char cmdline[MAX_GUEST_CMDLINE];

	DIR *dp;
	struct dirent *dirp;

	mount("xenfs","/proc/xen","xenfs",MS_NODEV,"");

    if((dp=opendir("/dev")) == NULL)
        exit(0);
    while((dirp = readdir(dp)) != NULL)
        fprintf(stderr,"%s ",dirp->d_name);
    fprintf(stderr,"\n");
    closedir(dp);

	xsh=xs_daemon_open();
	if(xsh==NULL){
		printf("Cannot open xenstore handle\n");
		exit(0);
	}

    /* Fetch argc, argv from XenStore */
    domid = xenbus_read_integer("target");
    if (domid == -1) {
        printf("Couldn't read target\n");
        exit(0);
    }

    snprintf(path, sizeof(path), "/local/domain/%d/vm", domid);
    vm = xs_read(xsh,XBT_NULL, path,&len);
    if (!len) {
        printf("Couldn't read vm path\n");
        exit(0);
    }

    snprintf(path, sizeof(path), "%s/image/dmargs", vm);
    free(vm);

    domargs = xs_read(xsh,XBT_NULL, path, &len);
    if (!len) {
        printf("stubdom args is empty\n");
        domargs = strdup("");
    }

	xs_daemon_close(xsh);

	argc = 1;

//	PARSE_ARGS_COUNT(cmdline);
	PARSE_ARGS_COUNT(domargs);

	argv = alloca((argc + 3) * sizeof(char *));
    argv[0] = "main";
    argv[1] = "-M";
    argv[2] = "xenfv";
    argc = 3;

//    PARSE_ARGS_STORE(cmdline)
    PARSE_ARGS_STORE(domargs)

    argv[argc] = NULL;

	execve("/bin/qemu",argv,envp);

	return 1;
}

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define ETH_ALEN 6
#define ARPHRD_ETHER 1
#define isdigit(a) ((unsigned char)((a) - '0') <= 9)

#ifndef IFNAMSIZ
#define IFNAMSIZ 16
#endif

int skfd = 0;
int err = 0;

/* Set a certain interface flag. */
static int set_flag(char *ifname, short flag)
{
    struct ifreq ifr;

    strncpy(ifr.ifr_name, ifname, IFNAMSIZ);
    if (ioctl(skfd, SIOCGIFFLAGS, &ifr) < 0) {
        fprintf(stderr,"%s: unknown interface: %s\n",
                ifname, strerror(errno));
	    return (-1);
    }
    strncpy(ifr.ifr_name, ifname, IFNAMSIZ);
    ifr.ifr_flags |= flag;
    if (ioctl(skfd, SIOCSIFFLAGS, &ifr) < 0) {
        perror("SIOCSIFFLAGS");
	    return -1;
    }
    return (0);
}

/* Clear a certain interface flag. */
static int clr_flag(char *ifname, short flag)
{
    struct ifreq ifr;

    strncpy(ifr.ifr_name, ifname, IFNAMSIZ);
    if (ioctl(skfd, SIOCGIFFLAGS, &ifr) < 0) {
	    return -1;
    }
    strncpy(ifr.ifr_name, ifname, IFNAMSIZ);
    ifr.ifr_flags &= ~flag;
    if (ioctl(skfd, SIOCSIFFLAGS, &ifr) < 0) {
	    return -1;
    }

    return (0);
}

unsigned hexchar2int(char c)
{
	if (isdigit(c))
		return c - '0';
	c &= ~0x20; /* a -> A */
	if ((unsigned)(c - 'A') <= 5)
		return c - ('A' - 10);
	return ~0U;
}

/* Input an Ethernet address and convert to binary. */
int ether_input(const char *bufp, struct sockaddr *sap)
{
	unsigned char *ptr;
	char c;
	int i;
	unsigned val;

	sap->sa_family = ARPHRD_ETHER;
	ptr = (unsigned char*) sap->sa_data;

	i = 0;
	while ((*bufp != '\0') && (i < ETH_ALEN)) {
		val = hexchar2int(*bufp++) * 0x10;
		if (val > 0xff) {
			errno = EINVAL;
			return -1;
		}
		c = *bufp;
		if (c == ':' || c == 0)
			val >>= 4;
		else {
			val |= hexchar2int(c);
			if (val > 0xff) {
				errno = EINVAL;
				return -1;
			}
		}
		if (c != 0)
			bufp++;
		*ptr++ = (unsigned char) val;
		i++;

		/* We might get a semicolon here - not required. */
		if (*bufp == ':') {
			bufp++;
		}
	}
	return 0;
}


int main(int argc, char **argv)
{
    struct sockaddr sa;
    struct ifreq ifr;
    int goterr = 0, didnetmask = 0;
    char **spp,host[128];
    int fd;

    /* Create a channel to the NET kernel. */
    if ((skfd = socket(AF_LOCAL, SOCK_DGRAM, 0)) < 0) {
	    exit(1);
    }

    /* No. Fetch the interface name. */
    spp = argv;
    spp++;
    strncpy(ifr.ifr_name, *spp++, IFNAMSIZ);
    if (*spp == (char *) NULL) {
	    (void) close(skfd);
	    exit(err < 0);
    }

    /* Process the remaining arguments. */
    while (*spp != (char *) NULL) {

	    if (!strcmp(*spp, "up")) {
	        goterr |= set_flag(ifr.ifr_name, (IFF_UP | IFF_RUNNING));
	        spp++;
	        continue;
	    }

	    if (!strcmp(*spp, "down")) {
	        goterr |= clr_flag(ifr.ifr_name, IFF_UP);
	        spp++;
	        continue;
	    }

	    if (!strcmp(*spp, "mac")) {
	        if (*++spp == NULL)
		        exit(1);
	        strncpy(host, *spp, (sizeof host));
	        if (ether_input(host, &sa) < 0) {
		        goterr = 1;
		        spp++;
		        continue;
	        }
	        memcpy((char *) &ifr.ifr_hwaddr, (char *) &sa, sizeof(struct sockaddr));
	        if (ioctl(skfd, SIOCSIFHWADDR, &ifr) < 0) {
		        goterr = 1;
	        }
	        spp++;
	        continue;
	    }
	    spp++;
    }

    exit(0);
}


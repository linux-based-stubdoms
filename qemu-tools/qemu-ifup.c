#include <stdio.h>
#include <stdlib.h>
#include <xs.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mount.h>

#include <dirent.h>

#define BRIDGE_PATH "/sys/class/net/xenbr0"

void main(int argc, char *argv[])
{
    DIR *dp;
    pid_t pid;

    if((dp=opendir(BRIDGE_PATH)) == NULL){
		fprintf(stderr,"there is no %s dir\n",BRIDGE_PATH);
		exit(0);
	}

    if((pid = fork()) == 0){
        execl("/bin/netconfig","netconfig",argv[1],"up",(char *)0);
    }
    while(waitpid(pid, NULL, 0) != pid) {
        /* loop */
    }

    if((pid = fork()) == 0)
        execl("/bin/brctl","brctl","addif","xenbr0",argv[1]);
    while(waitpid(pid, NULL, 0) != pid) {
        /* loop */
    }

    //for debugging
    if((pid = fork()) == 0)
        execl("/bin/brctl","brctl","show",(char *)0);
    while(waitpid(pid, NULL, 0) != pid) {
        /* loop */
    }

    exit(0);
}
